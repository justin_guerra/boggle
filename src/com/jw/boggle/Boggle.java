package com.jw.boggle;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.IntStream;

public class Boggle {

	public static void main(String[] args) throws IOException, InterruptedException {
		final boolean newBoard = true;

		final Board board;
		if(newBoard)
		{
			final int size = 256;
			board = Board.newBoard(size);
//			Board.writeBoard(board, "trial.txt");
		}
		else
		{
			board = Board.loadBoard("trial.txt");
		}
		
		System.out.println("Finding solutions on board of size " + board.getSize());
		
		final long start = System.nanoTime();
		int[] list = board.newAdjacencyList();
		Dictionary dict = Dictionary.buildDict("en_US.dic");
		ParallelSolver solver = new ParallelSolver(board, dict, list);
		solver.solve();
		final long end = System.nanoTime() - start;

		System.out.println("Found " + solver.getSolutions().size() + " solutions in " + end / 1_000_000 + " ms");
//		solver.getSolutions().stream().sorted((x,y) -> y.length() - x.length()).forEach(System.out::println);
	}
	
	private static final class ParallelSolver
	{
		private static final int SIGN_BIT = 1 << 31;
		
		private final Set<String> solutions;
		private final Board board;
		private final Dictionary dict;
		private final int[] list;
		private final ThreadLocal<int[]> boardData;
		
		public ParallelSolver(final Board board, Dictionary dict, final int[] adjacencyList) {
			this.solutions = Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>(18000));
			this.board = board;
			this.dict = dict;
			this.list = adjacencyList;
			this.boardData = ThreadLocal.withInitial(() -> Arrays.copyOf(board.getBoard(), board.getBoard().length));
		}

		public Set<String> getSolutions() {
			return solutions;
		}

		public void solve() throws InterruptedException
		{
			final int totalSize = board.getSize() * board.getSize();
			final int[] gameData = board.getBoard();
			
			IntStream.range(0, totalSize)
					 .parallel()
					 .forEach(x -> 
						 {
							 MutableString word = new MutableString((char)gameData[x]);
							 int[] list = boardData.get();
							 list[x] = list[x] | SIGN_BIT;
							 search(x, word);
							 list[x] = list[x] ^ SIGN_BIT;
						 });
			
		}
		
		private void search(final int startIndex, final MutableString word)
		{
			if(dict.isWord(word))
			{
				solutions.add(word.toString());
			}
			
			if(!dict.isPartial(word))
			{
				return;
			}
			
			final int baseIndex = startIndex * 8;
			final int[] data = boardData.get();
			for(int i = 0; i < 8 && list[baseIndex + i] != -1; ++i)
			{
				final int nextIndex = list[baseIndex + i];
				
				if(data[nextIndex] < 0) //flipping sign bit to determine if value has already been visited
				{
					continue;
				}
				
				word.append((char)board.getBoard()[nextIndex]);
				data[nextIndex] = data[nextIndex] | SIGN_BIT;
				search(nextIndex, word);
				data[nextIndex] = data[nextIndex] ^ SIGN_BIT;
				word.removeLast();
			}
		}
	}
}
