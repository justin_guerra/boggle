package com.jw.boggle;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Dictionary {
	private static final int[] partials = {5,6,7};
	
	private final Set<MutableString> lookup;
	private final Set<MutableString> partialLookup;

	private Dictionary(final Set<MutableString> lookup, final Set<MutableString> partialLookup) {
		
		this.lookup = lookup;
		this.partialLookup = partialLookup;
	}

	public final boolean isWord(final MutableString trial)
	{
		return lookup.contains(trial);
	}
	
	public final boolean isPartial(final MutableString trial)
	{
		return trial.length() <= partials[0] || partialLookup.contains(trial);
	}
	
	public static Dictionary buildDict(final String dictName) throws IOException
	{
		final Path dictPath = Paths.get(dictName);
		final List<String> allLines = Files.readAllLines(dictPath);

		final Set<MutableString> words = new HashSet<>(46614);

		final Set<MutableString> partialSet = new HashSet<>(66592);
		
		for(String s: allLines)
		{
			if(s.length() < 3)
			{
				continue;
			}
			
			final char[] chars = s.toLowerCase().toCharArray();
			updatePartials(partialSet, chars);
			words.add(new MutableString(chars, chars.length));
		}

		return new Dictionary(words, partialSet);
	}

	private static void updatePartials(Set<MutableString> set, char[] word)
	{
		for(int i = 0; i < partials.length; ++i)
		{
			if(word.length < partials[i])
			{
				return;
			}
			
			set.add(new MutableString(word, partials[i])); //word.substring(0, partials[i]));
		}
	}
}
