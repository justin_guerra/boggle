package com.jw.boggle;


public class MutableString {

	private static final int MAX_SIZE = 23; //longest word in dictionary
	
	private final char[] data;
	
	private int size;
	
	public MutableString(final char c) {
		data = new char[MAX_SIZE];
		data[size++] = c;
	}
	
	public MutableString(final char[] data, final int size)
	{
		this.data = data;
		this.size = size;
	}
	
	public void clear()
	{
		size = 0;
//		Arrays.fill(data, '\0');
	}

	public void append(final char c)
	{
		data[size++] = c;
	}

	public int length()
	{
		return size;
	}
	
	public void removeLast()
	{
		size--;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		
		for (int i = 0; i < size; ++i) {
			result = prime * result + data[i]; //from Arrays.hashCode()
		}
		
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		//only compare other mutable strings
		final MutableString other = (MutableString) obj;
		
		if(size != other.size)
		{
			return false;
		}
		
		for(int i = 0; i < size; ++i)
		{
			if(data[i] != other.data[i])
			{
				return false;
			}
		}
		
		return true;
	}

	@Override
	public String toString()
	{
		return new String(data, 0, size);
	}
}
